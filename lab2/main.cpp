#include <iostream>
#include "matrixmul.h"
using namespace std;

int main(){
    int N;
    cin >> N;
    double** A;
    double** B;
    double** C;
    double** D;
	double** E;
    A = new double*[N];
    for(int i = 0; i < N; i ++)
        A[i] = new double[N];
    B = new double*[N];
    for(int i = 0; i < N; i ++)
        B[i] = new double[N];
    C = new double*[N];
    for(int i = 0; i < N; i ++)
        C[i] = new double[N];
    D = new double*[N];
    for(int i = 0; i < N; i ++)
        D[i] = new double[N];
	E = new double*[N];
    for(int i = 0; i < N; i ++)
        E[i] = new double[N];

    cin_matrix(A, N, N);
    cin_matrix(B, N, N);

    //GEMM
    clock_t start,end;//定义clock_t变量
    start = clock();//开始时间
    if(N<=2048)general_mul(A,B,C,N,N,N);
    end = clock();//结束时间
    cout<<"GEMM's time = "<<double(end-start)/CLOCKS_PER_SEC<<"s"<<endl;//输出时间（单位：ｓ）
    if(N<=8)
    cout_matrix(C, N, N);

    //Strassen
    start = clock();//开始时间
    if(N<=2048)Strassen(N,A,B,D);
    end = clock();//结束时间
    cout<<"Strasen's time = "<<double(end-start)/CLOCKS_PER_SEC<<"s"<<endl;//输出时间（单位：ｓ）
    if(N<=8)
    cout_matrix(D, N, N);

	//Based on software optimization
	start = clock();//开始时间
    based_on_software_mul(A,B,E,N,N,N);
    end = clock();//结束时间
    cout<<"Based on software's time = "<<double(end-start)/CLOCKS_PER_SEC<<"s"<<endl;//输出时间（单位：ｓ）
    if(N<=8)
    cout_matrix(E, N, N);

}