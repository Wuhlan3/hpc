#ifndef MATRIXMUL_H__
#define MATRIXMUL_H__
extern void Strassen(int N, double** MatrixA, double** MatrixB, double** MatrixC);
extern void based_on_software_mul(double** A, double ** B, double ** C, int m, int n, int k);
extern void general_mul(double** A, double ** B, double ** C, int m, int n, int k);
extern void cout_matrix(double ** C, int row, int col);
extern void cin_matrix(double ** A, int row, int col);
#endif
